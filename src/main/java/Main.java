public class Main {
    public static void main(String[] args) {
        int[][] lane_rouge = {
                {1, 2, 2, 3},
                {1, 2, 2, 3},
                {4, 5, 5, 6},
                {4, 7, 8, 6},
                {9, 0, 0, 10}
        };
        Board initial = new Board(lane_rouge);
        Solver solver = new Solver(initial);

        System.out.println("Minimum number of moves = " + solver.moves());
        for (Board board : solver.solution())
            System.out.println(board);
    }
}
