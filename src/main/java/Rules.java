public class Rules {
    //Массивы размеров костяшек, где индекс - номер костяшки
    private static final int[] width = {1, 1, 2, 1, 1, 2, 1, 1, 1, 1, 1};
    private static final int[] height = {1, 2, 2, 2, 2, 1, 2, 1, 1, 1, 1};

    //Проверка, можно ли сдвинуть костяшку
    public boolean isMovable(int[][] matrix, int string1, int column1, int string2, int column2){
        //Первыми идут координаты нуля, потом координаты костяшки
        if (matrix[string1][column1]!=0){
            return false;
        }
        if (matrix[string2][column2]==0){
            return false;
        }
        //движение по горизонтали
        if (string1==string2){
            //Если высота костяшки 1, ее очевидно можно сдвинуть. Если же 2, то движение возможно если под нашим 0 есть еще 0, а часть костяшки на string2 - верхняя
            if (height[matrix[string2][column2]]==1) {
                return true;
            } else if (string1+1<5){
                if ((matrix[string1+1][column1]==0)&&(matrix[string2][column2]==matrix[string2+1][column2]))
                    return  true;
            }
        }
        //движение по вертикали
        if (column1==column2){
            //Если ширина костяшки 1, ее очевидно можно сдвинуть. Если же 2, то движение возможно если справа от нашего 0 есть еще 0, а часть костяшки на column2 - левая
            if (width[matrix[string2][column2]]==1) {
                return true;
            } else if (column1+1<4){
                if ((matrix[string1][column1+1]==0)&&(matrix[string2][column2]==matrix[string2][column2+1]))
                    return  true;
            }
        }
        return false;
    }

    private void swap(int[][] matrix, int string1, int column1, int string2, int column2){
        int temp = matrix[string1][column1];
        matrix[string1][column1] = matrix[string2][column2];
        matrix[string2][column2] = temp;
    }


    public int[][] move(int[][] matrix, int string1, int column1, int string2, int column2){
        //Рабочая матрица
        int[][] tempMatrix=new int[5][4];
        for (int i=0; i<matrix.length; i++){
            for (int j=0; j<matrix[i].length; j++){
                tempMatrix[i][j]=matrix[i][j];
            }
        }
        //если движение по горизонтали
        if (string1==string2){
            //Если ширина костяшки 1, меняем местами ее и 0, в случае когда она высотой 2 - еще и ее нижнюю часть и ноль снизу
            if (width[tempMatrix[string2][column2]]==1){
                swap(tempMatrix, string1, column1,string2, column2);
                if (height[tempMatrix[string2][column2]]==2){
                    swap(tempMatrix, string1+1, column1,string2+1, column2);
                }
            }
            //Если ширина костяшки 2, меняем местами ее дальнюю часть и 0, в случае когда она высотой 2 - еще и ее нижнюю дальнюю часть и ноль снизу
            if (width[tempMatrix[string2][column2]]==2){
                swap(tempMatrix, string1, column1,string2, 2*column2-column1);
                if (height[tempMatrix[string2][column2]]==2){
                    swap(tempMatrix, string1+1, column1,string2+1, 2*column2-column1);
                }
            }
        }
        //если движение по вертикали
        if (column1==column2){
            if (height[tempMatrix[string2][column2]]==1){
                swap(tempMatrix, string1, column1,string2, column2);
                if (width[tempMatrix[string2][column2]]==2){
                    swap(tempMatrix, string1, column1+1,string2, column2+1);
                }
            }
            if (height[tempMatrix[string2][column2]]==2){
                swap(tempMatrix, string1, column1,2*string2-string1, column2);
                if (width[tempMatrix[string2][column2]]==2){
                    swap(tempMatrix, string1, column1,2*string2-string1, column2+1);
                }
            }
        }
        return tempMatrix;
    }
}
