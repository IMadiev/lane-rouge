import java.util.HashSet;
import java.util.Set;

public class Board {
    private int[][] matrix; // изображение доски в текущий момент времени
    private int h; //эвристическое приближение стоимости пути от  нынешней до конечной цели
    private static final int GOAL_COLUMN = 1; // целевое местоположение левой верхней клетки "осла"
    private static final int GOAL_STRING = 3;
    private int zero1_column; //координаты пустых клеток
    private int zero2_column;
    private int zero1_string;
    private int zero2_string;

    public Board(int[][] matrix){
        //проверка размера матрицы
        assert matrix.length == 5;
        for (int i=0; i<matrix.length; i++) {
            assert matrix[i].length == 4;
        }

        int[][] tempMatrix=new int[5][4]; //матрица с которой будем работать
        int rouge_column = 0; //координаты левой верхней клетки осла
        int rouge_string = 0;
        int zero_counter=0;  //счетчик нулей, для нахождения их координат
        
        //копирование исходной матрицы в рабочую, поиск местонахождения осла для эвристики, поиск координат нулей
        for (int i=0; i<matrix.length; i++){
            for (int j=0; j<matrix[i].length; j++){
                tempMatrix[i][j]=matrix[i][j];
                if (matrix[i][j]==2){
                    rouge_column = j;
                    rouge_string = i;
                }
                if (matrix[i][j]==0){
                    if (zero_counter==0){
                        zero1_column=j;
                        zero1_string=i;
                    }
                    if (zero_counter==1){
                        zero2_column=j;
                        zero2_string=i;
                    }
                }
            }
        }
        this.matrix = tempMatrix;
        //Выбранная эвристика - манхэттенское расстояние
        h=Math.abs(rouge_column-1-GOAL_COLUMN)+Math.abs(rouge_string-1-GOAL_STRING);
    }
    
    public int h(){
        return h;
    }

    public boolean isGoal(){
        return h==0;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o==null||getClass()!=o.getClass()) {
            return false;
        }

        Board board = (Board) o;

        for (int i=0; i<matrix.length; i++){
            for (int j=0; j<matrix[i].length; j++){
                if (matrix[i][j]!=board.matrix[i][j]){
                    return false;
                }
            }
        }
        return true;
    }

    //для каждого нуля пробуем передвинуть в него соседнюю костяшку. Правила перемещения в классе Rules. В случае невозможности движения возвращается null
    public Iterable<Board> neighbors(){
        Set<Board> boardList = new HashSet<Board>();
        boardList.add(chng(copyMatrix(), zero1_string, zero1_column, zero1_string, zero1_column+1));
        boardList.add(chng(copyMatrix(), zero1_string, zero1_column, zero1_string, zero1_column-1));
        boardList.add(chng(copyMatrix(), zero1_string, zero1_column, zero1_string+1, zero1_column));
        boardList.add(chng(copyMatrix(), zero1_string, zero1_column, zero1_string-1, zero1_column));
        boardList.add(chng(copyMatrix(), zero2_string, zero2_column, zero2_string, zero2_column+1));
        boardList.add(chng(copyMatrix(), zero2_string, zero2_column, zero2_string, zero2_column-1));
        boardList.add(chng(copyMatrix(), zero2_string, zero2_column, zero2_string+1, zero2_column));
        boardList.add(chng(copyMatrix(), zero2_string, zero2_column, zero2_string-1, zero2_column));
        return boardList;
    }

    private int[][] copyMatrix(){
        int[][] tempMatrix=new int[5][4];
        for (int i=0; i<matrix.length; i++){
            for (int j=0; j<matrix[i].length; j++){
                tempMatrix[i][j]=matrix[i][j];
            }
        }
        return tempMatrix;
    }

    private Board chng(int[][] tempMatrix, int string1, int column1, int string2, int column2) {
        //если если клетка, которую мы хотим поменять местами с нулем за пределами доски - возвращаем null
        if (string2 > -1 && string2 < 5 && column2 > -1 && column2 < 4) {
            Rules rules = new Rules();
            //Если костяшку нельзя сдвинуть согласно правилам - возвращаем null
            if (!rules.isMovable(tempMatrix, string1, column1, string2, column2)) {
                return null;
            }
            return new Board(rules.move(tempMatrix, string1, column1, string2, column2));
        } else
            return null;

    }

    public String toString(){
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                s.append(String.format("%2d ", matrix[i][j]));
            }
            s.append("n");
        }
        return s.toString();
    }
}
