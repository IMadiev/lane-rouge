import org.junit.Assert;
import org.junit.Test;

public class RulesTest {
    private int[][] lane_rouge = {{1, 2, 2, 3}, {1, 2, 2, 3}, {4, 5, 5, 6}, {4, 7, 8, 6}, {9, 0, 0, 10}};
    private Rules rules = new Rules();

    @Test
    public void isMovable_positive_1(){
        Assert.assertTrue(rules.isMovable(lane_rouge, 4, 1, 4, 0));
        Assert.assertTrue(rules.isMovable(lane_rouge, 4, 2, 4, 3));
        Assert.assertTrue(rules.isMovable(lane_rouge, 4, 1, 3, 1));
        Assert.assertTrue(rules.isMovable(lane_rouge, 4, 2, 3, 2));
    }

}
